# Tecnologias usadas

- Laravel 8.x
- Bootstrap 5
- jQuery
- Banco de dados: MySQL

# Como executar o projeto

```bash
# clonar repositório
git clone https://RafaelLima99@bitbucket.org/RafaelLima99/unu-estagio-dev.git

# Instalar dependências do projeto
composer install  

# Criar banco de dados no PHPMyAdmin com o nome: db_unu

# rodar o comando: php artisan migrate

# inserir dados na tabela series
INSERT INTO series (serie) VALUES ('1º ano'), ('2º ano'), ('3º ano') 

# executar o projeto
php artisan serve

# criar conta e fazer login 
```
# Autor
- Rafael Lima dos Santos Cabral
- E-mail: rafaellima.ti.99@gmail.com